package com.snowypeaksystems.test;

import com.snowypeaksystems.game.Freddy;
import com.snowypeaksystems.game.Map;
import com.snowypeaksystems.game.Player;

/**
 * Created by Levi Muniz on 7/15/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class TestScriptedGame {
    public static void main(String[] args) throws InterruptedException {
        Freddy freddy = new Freddy();
        Player player = new Player();
        Map map = new Map(3);
        map.addAnimatronic(freddy);
        map.setPlayer(player);

        map.start();

        while (player.isAlive()) {
            if (!player.checkCamera(0).isEmpty()) {
                player.toggleDoor(0);
            } else if (!player.checkCamera(1).isEmpty()) {
                player.toggleDoor(1);
            } else {
                player.toggleDoor(2 );
            }

            while (!player.wasAttacked() && player.isAlive()) {
                Thread.sleep(1000);
            }
        }
    }
}
