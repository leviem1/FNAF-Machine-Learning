package com.snowypeaksystems.test;

import com.snowypeaksystems.game.Freddy;
import com.snowypeaksystems.game.Map;
import com.snowypeaksystems.game.Player;

import java.util.Scanner;

/**
 * Created by Levi Muniz on 7/7/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class TestInteractiveGame {
    public static void main(String[] args) {
        Freddy freddy = new Freddy();
        Player player = new Player();
        Map map = new Map(3);
        map.addAnimatronic(freddy);
        map.setPlayer(player);

        map.start();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            String input = scanner.nextLine().trim().toUpperCase();

            if (player.isAlive()) {
                switch (input) {
                    case "1":
                        player.checkCamera(0);
                        break;
                    case "2":
                        player.checkCamera(1);
                        break;
                    case "3":
                        player.checkCamera(2);
                        break;
                    case "Q":
                        player.toggleDoor(0);
                        break;
                    case "W":
                        player.toggleDoor(1);
                        break;
                    case "E":
                        player.toggleDoor(2);
                        break;
                }

            } else {
                break;
            }
        }
    }
}
