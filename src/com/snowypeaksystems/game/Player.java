package com.snowypeaksystems.game;

import com.snowypeaksystems.utils.LoggerWrapper;

import java.util.Set;
import java.util.logging.*;

/**
 * Created by Levi Muniz on 7/7/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class Player {
    private Map map;
    private boolean attacked = false;
    private boolean winner = false;
    private static final Logger LOGGER = LoggerWrapper.getLogger(Player.class.getName());

    void addMap(Map m) {
        map = m;
    }

    public Set<Animatronic> checkCamera(int num) {
        Set<Animatronic> animatronics = map.checkCamera(num);
        LOGGER.info("Animatronics at camera " + num + ": " + animatronics);

        return animatronics;
    }

    public void toggleDoor(int doorNum) {
        map.doorDown(doorNum);
    }

    public boolean isAlive() {
        return map.isActive();
    }

    void setAttacked() {
        attacked = true;
    }

    public boolean wasAttacked() {
        boolean result = attacked;
        attacked = false;

        return result;
    }

    public boolean isWinner() {
        return winner;
    }

    void setWinner(boolean winner) {
        this.winner = winner;
    }
}
