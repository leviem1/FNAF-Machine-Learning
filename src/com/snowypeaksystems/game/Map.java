package com.snowypeaksystems.game;

import com.snowypeaksystems.utils.LoggerWrapper;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.*;

/**
 * Created by Levi Muniz on 7/7/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class Map {
    private Timer timer;
    private Player player;
    private int numCameras;
    private boolean isActive;
    private List<Boolean> doors;
    private ArrayList<Animatronic> animatronics;
    private ArrayList<Set<Animatronic>> cameras;
    private static final Logger LOGGER = LoggerWrapper.getLogger(Map.class.getName());

    public Map(int numCameras) {
        this.numCameras = numCameras;

        timer = new Timer("Clock");

        cameras = new ArrayList<>();
        animatronics = new ArrayList<>();
        doors = Collections.synchronizedList(new ArrayList<>());

        for (int i = 0; i < numCameras; i++) {
            Set<Animatronic> set = ConcurrentHashMap.newKeySet();
            cameras.add(set);

            doors.add(false);
        }

        LOGGER.info("Map initialized");
    }

    public void addAnimatronic(Animatronic a) {
        a.setMap(this);
        animatronics.add(a);

        LOGGER.info("Added " + a.getClass().getName());
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.player.addMap(this);
    }

    Set<Animatronic> checkCamera(int camNum) {
        return cameras.get(camNum);
    }

    void moveAnimatronic(int lastCamera, int newCamera, Animatronic a) {
        if (lastCamera > -1) {
            cameras.get(lastCamera).remove(a);
        }

        cameras.get(newCamera).add(a);
    }

    synchronized void doorDown(int doorNum) {
        for (int i = 0; i < numCameras; i++) {
            doors.set(i, false);
        }

        doors.set(doorNum, true);

        LOGGER.info("Door " + doorNum + " down");
    }

    synchronized void attackPlayer(Animatronic a) {
        if (!doors.get(a.getDoor())) {
            LOGGER.info(a + " attacked you!");
            stop(false);
        } else {
            LOGGER.info(a + " failed to attack you");
            player.setAttacked();
        }
    }

    int getNumCameras() {
        return numCameras;
    }

    boolean isActive() {
        return isActive;
    }

    public void start() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                stop(true);
                LOGGER.info("Stopped Map");
            }
        };

        for (Animatronic a : animatronics) {
            a.start();
            LOGGER.info("Started " + a.getClass().getName());
        }

        timer.schedule(task,  360000L);

        isActive = true;

        LOGGER.info("Game Started");
    }

    private void stop(boolean isWin) {
        isActive = false;
        player.setWinner(isWin);

        if (isWin) {
            LOGGER.info("Game Win!");
        } else {
            LOGGER.info("Game Lose");
            LOGGER.info("Camera states: " + cameras.get(0) + " " + cameras.get(1) + " " + cameras.get(2));
        }

        for (Animatronic a : animatronics) {
            a.stop();
            LOGGER.info("Stopped " + a.getClass().getName());
        }

        timer.cancel();
        timer.purge();
    }


}
