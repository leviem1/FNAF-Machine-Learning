package com.snowypeaksystems.game;

import java.util.Random;

/**
 * Created by Levi Muniz on 7/7/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class Freddy implements Animatronic {
    private Map map;
    private volatile int lastCamera;
    private int numCameras;
    private Random rand = new Random();
    private Thread movementThread;

    private synchronized void move() {
        int nextCamera = rand.nextInt(numCameras);

        map.moveAnimatronic(lastCamera, nextCamera, this);

        lastCamera = nextCamera;
    }

    @Override
    public synchronized int getDoor() {
        return lastCamera;
    }

    @Override
    public void setMap(Map m) {
        map = m;
        numCameras = map.getNumCameras();
    }

    @Override
    public void start() {
        int nextCamera = rand.nextInt(numCameras);

        map.moveAnimatronic(-1, nextCamera, this);

        lastCamera = nextCamera;

        movementThread = new Thread(() -> {
            Random randSecs = new Random();

            while (!Thread.interrupted()) {
                int seconds = randSecs.nextInt(11) + 5;

                try {
                    Thread.sleep(seconds * 1000);
                    map.attackPlayer(this);

                    move();
                } catch (InterruptedException e) {
                    break;
                }
            }
        });

        movementThread.start();
    }

    @Override
    public void stop() {
        movementThread.interrupt();
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
