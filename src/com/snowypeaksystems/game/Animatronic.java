package com.snowypeaksystems.game;

/**
 * Created by Levi Muniz on 7/7/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public interface Animatronic {
    int getDoor();

    void setMap(Map m);

    void start();

    void stop();
}

//TODO: Remake as an object with basic move, start(?), and stop(?) mechanics