package com.snowypeaksystems.utils;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by Levi Muniz on 7/10/18.
 * Copyright (c) Levi Muniz. All Rights Reserved.
 */
public class LoggerWrapper {
    private static Logger LOGGER;

    public static Logger getLogger(String name) {
        if (LOGGER == null) {
            LOGGER = Logger.getLogger(LoggerWrapper.class.getName());

            Handler console = new ConsoleHandler();
            Formatter simpleFormatter = new SimpleFormatter();
            console.setFormatter(simpleFormatter);
            //LOGGER.addHandler(console);

            try {
                Handler file = new FileHandler("game_events.log");
                file.setFormatter(simpleFormatter);
                LOGGER.addHandler(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Logger childLogger = Logger.getLogger(name);
        childLogger.setParent(LOGGER);

        return childLogger;
    }
}
